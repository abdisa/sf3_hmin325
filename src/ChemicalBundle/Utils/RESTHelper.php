<?php

namespace ChemicalBundle\Utils;

/**
 * Class REST Helper.
 *
 * @author Aurélien Muller CGI, Abdoulaye DIALLO
 * permet de creeer un service qui va télécharger les elements chimiques periodiques de Mendelew
 */
class RESTHelper
{
    /**
     * Gets data from periodic table of chemical elements.
     *
     * Il s'agit d'un format json "formule" => Nom.
     * Consomme un webservice REST (GET).
     *
     * @return mixed
     */
    public function getPeriodicElements()
    {
        // That URL is used to get the list of chemical elements :
        // key: formula; value : name.
        $serviceUrl = "https://nickclifford.me/api/pt.php?mode=names";
        return $this->get($serviceUrl, 'json');
    }

    /**
     * Sends a GET request on service url.
     *
     * @param type $serviceUrl
     * @param type $format
     *
     * @return mixed
     */
    public function get($serviceUrl, $format = 'json')
    {
        $curl = \curl_init($serviceUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $curlResponse = curl_exec($curl);
        curl_close($curl);

        // Return type will be array.
        if ($format == 'json') {
            $curlResponse = json_decode($curlResponse, true);
        }

        return $curlResponse;
    }
}
