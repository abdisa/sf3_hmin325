<?php
namespace ChemicalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalBundle\Entity\Atom;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;


/**
 * Atom controller.
 *
 * @author Abdoulaye DIALLO
 */
class AtomController extends Controller
{

  public function listAction($page, $maxPerPage)
  {
    $rep = $this->getDoctrine()->getRepository("ChemicalBundle:Atom");
    $atom = $rep->getListPaginated($page, $maxPerPage);
    //definition de la variable paginator.
    $pagination = array(
                        'page' => $page,
                        'nbPages' => ceil(count($atom) / $maxPerPage),
                        'max' => $maxPerPage,
                        'routeName' => 'chemical_atom_list',
                        'routeParams' => array()
                      );
    return $this->render('chemical/atom.list.html.twig',
                            [
                            'atoms' => $atom,
                            'pagination' => $pagination,
                            ]);
  }

  public function seeAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    // On récupère l'atom $id
    $atom = $em
      ->getRepository('ChemicalBundle:Atom')
      ->find($id)
        ;

    if (null === $atom) {
        throw new NotFoundHttpException("L'atom demandé d'id ".$id." n'existe pas.");
    }

    return $this->render('chemical/atom.see.html.twig', array(
            'atom' => $atom
         ));
  }
}
