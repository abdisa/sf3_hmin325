<?php
namespace ChemicalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalBundle\Entity\Element;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;


/**
 * Element controller.
 *
 * @author Abdoulaye DIALLO
 */
class ElementController extends Controller
{
   public function aproposAction()
    {
        // Root path of render is app/Resources/views.
        return $this->render('chemical/element.apropos.html.twig');
    }

    /**
     * Display elements from periodic table.
     * Autre facon facon de faire de la pagination.
     * @param type $page
     * @param type $maxPerPage
     *
     * @return type
     */
    public function listAction($page, $maxPerPage)
    {
      $rep = $this->getDoctrine()->getRepository("ChemicalBundle:Element");
      $element = $rep->getListPaginated($page, $maxPerPage);
      //definition de la variable paginator.
      $pagination = array(
                          'page' => $page,
                          'nbPages' => ceil(count($element) / $maxPerPage),
                          'max' => $maxPerPage,
                          'routeName' => 'chemical_element_list',
                          'routeParams' => array()
                        );
      return $this->render('chemical/element.list.html.twig',
                              [
                              'elements' => $element,
                              'pagination' => $pagination,
                              ]);
    }

    /**
     * Asks a webservice for data about elements from Mendeleiev table.
     *
     * This data is then used to create entities: elements.
     */
    public function createSampleElementsFromWebserviceAction()
    {
        try {
            // Let's get the RESTHelper service.
            $ws = $this->get("chemical.helper");
            $periodicElements = $ws->getPeriodicElements();

            if (!empty($periodicElements)) {
                // Doctrine Manager is used to
                $em = $this->getDoctrine()->getManager();
                foreach ($periodicElements as $formula => $name) {
                    // Declaration of a new Element entity.
                    $element = new Element();
                    $element->setFormula($formula);
                    $element->setName($name);

                    // Index the entity within Doctrine.
                    // That method DOES NOT launch any request
                    // to MYSQL server.
                    $em->persist($element);
                }
                // Flush method sends all the stored requests to MySQL.
                $em->flush();
                // AddFlash is a method storing messages that are displayed once.
                // Check documentation to know how to display them within Twig template
                // (base.html.twig)
                $this->addFlash(
                    'notice',
                    'Atoms and periodic elements were created successfully.'
                );
            } else {
                $this->addFlash(
                    'warning',
                    'Nothing was acquired from webservice.'
                );
            }
        } catch (Exception $ex) {
            $this->addFlash(
                'error',
                'An error occurred while creating elements. Check the logs for more information.'
            );
            // Log that error.
            // $this->get('logger')->error($ex->getMessage());
        }

        // Once the process is done, redirect the user to the page listing
        // all the elements contained within the database.
        // RedirectToRoute has for parameter the key of a route,
        // found in Resources/config/route.yml
        return $this->redirectToRoute("chemical_element_list");
    }

    public function seeAction($id)
    {
      $em = $this->getDoctrine()->getManager();
	    // On récupère l'element $id
	    $element = $em
    		->getRepository('ChemicalBundle:Element')
    		->find($id)
    	    ;

	    if (null === $element) {
		      throw new NotFoundHttpException("L'element demandé d'id ".$id." n'existe pas.");
	    }

	    return $this->render('chemical/element.see.html.twig', array(
				      'element' => $element
			     ));
    }


}
