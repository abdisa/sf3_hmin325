<?php

namespace ChemicalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Default controller.
 *
 * @author Abdoulaye DIALLO
 */
class DefaultController extends Controller
{
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();
      // On récupère les elements
      
      $elements = $em
        ->getRepository('ChemicalBundle:Element')
        ->findAll()
          ;
        $atoms = $em
          ->getRepository('ChemicalBundle:Atom')
          ->findAll()
            ;
        $molecules = $em
          ->getRepository('ChemicalBundle:Molecule')
          ->findAll()
            ;


        // Root path of render is app/Resources/views.
        return $this->render('chemical/index.html.twig', array(
  				      'elements' => $elements,
                'atoms' => $atoms,
                'molecules' => $molecules
  			     ));
    }
}
