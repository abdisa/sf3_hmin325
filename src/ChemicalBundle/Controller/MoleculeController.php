<?php
namespace ChemicalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalBundle\Entity\Molecule;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;


/**
 * Molecule controller.
 *
 * @author Abdoulaye DIALLO
 */
class MoleculeController extends Controller
{

  public function listAction($page, $maxPerPage)
  {
    $rep = $this->getDoctrine()->getRepository("ChemicalBundle:Molecule");
    $molecule = $rep->getListPaginated($page, $maxPerPage);
    //definition de la variable paginator.
    $pagination = array(
                        'page' => $page,
                        'nbPages' => ceil(count($molecule) / $maxPerPage),
                        'max' => $maxPerPage,
                        'routeName' => 'chemical_molecule_list',
                        'routeParams' => array()
                      );
    return $this->render('chemical/molecule.list.html.twig',
                            [
                            'molecules' => $molecule,
                            'pagination' => $pagination,
                            ]);
  }

  public function seeAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    // On récupère la molecule $id
    $molecule = $em
      ->getRepository('ChemicalBundle:Molecule')
      ->find($id)
        ;

    if (null === $molecule) {
        throw new NotFoundHttpException("La molecule demandé d'id ".$id." n'existe pas.");
    }

    return $this->render('chemical/molecule.see.html.twig', array(
            'molecule' => $molecule
         ));
  }
}
