<?php
namespace ChemicalBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ChemicalBundle\Entity\Element;
use ChemicalBundle\Entity\Atom;
use ChemicalBundle\Entity\AtomsGroup;
use ChemicalBundle\Entity\Molecule;

/**
 * Fixture to load sample data.
 *
 * Implements ContainerAwareInterface to be able to get the
 * services.
 */
class LoadChemicalsData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        //$this->generateElements();
        $this->generateMolecule();
    }

    protected function generateElements()
    {
        $ws = $this->container->get("chemical.helper");
        $periodicElements = $ws->getPeriodicElements();

        if (!empty($periodicElements)) {
            $em = $this->container->get('doctrine')->getManager();
            foreach ($periodicElements as $formula => $name) {
                $element = new Element();
                $element->setFormula($formula);
                $element->setName($name);
                $em->persist($element);
            }
            $em->flush();
        }
    }

    protected function generateMolecule()
    {
        $em = $this->container->get('doctrine')->getManager();
        $elementRepository = $this
            ->container
            ->get('doctrine')
            ->getRepository('ChemicalBundle:Element');

        $molecule = new Molecule();
        $molecule->setFormula("CH4-NH2");
        $molecule->setName("Methane");

        /*$c = $elementRepository->findByFormula("C")[0]; //findOneByFormula("C");
        $h = $elementRepository->findByFormula("H")[0];
        $n = $elementRepository->findByFormula("N")[0];*/

        $c = $elementRepository->find(2);
        $h = $elementRepository->find(1);
        $n = $elementRepository->find(3);

        $cAtome = new Atom();
        $cAtome->setElement($c);
        $cAtome->setName($c->getName());

        $nAtome = new Atom();
        $nAtome->setElement($n);
        $nAtome->setName($n->getName());

        $atoms = [];
        $atoms2 = [];
            $atom2 = new Atom();
            $atom2->setElement($h);
            $atom2->setName($h->getName());
        $atoms2[] = $atom2;
        $atom2 = new Atom();
        $atom2->setElement($h);
        $atom2->setName($h->getName());
        $atoms2[] = $atom2;

        $atomsGroup = new AtomsGroup();
        $atomsGroup->setMainAtom($cAtome);
        $atomsGroup->setName("CH4");
        $atomsGroup2 = new AtomsGroup();
        $atomsGroup2->setName("NH2");
        $atomsGroup2->setMainAtom($nAtome);

        for ($i = 0; $i < 4; $i++) {
            $atoms[$i] = new Atom();
            $atoms[$i]->setElement($h);
            $atoms[$i]->setName($h->getName());
            $atoms[$i]->addAtomsGroup($atomsGroup);
        }
        foreach ($atoms as $atom) {
            $atomsGroup->addAtom($atom);
        }
        foreach ($atoms2 as $atom) {
            $atomsGroup2->addAtom($atom);
        }
        $atomsGroup->setMolecule($molecule);
        $atomsGroup->setPosition(1);
        $atomsGroup2->setMolecule($molecule);
        $atomsGroup2->setPosition(2);

        $em->persist($atomsGroup);
        $em->persist($atomsGroup2);
        $em->flush();
    }
}

/*
LoadElement.php


<?php

namespace ChemicalBundle\DataFixtures\ORM;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ChemicalBundle\Entity\Element;


class LoadElement implements FixtureInterface, ContainerAwareInterface
{
  /**
   * @var ContainerInterface
   *
  private $container;

  public function setContainer(ContainerInterface $container = null)
  {
      $this->container = $container;
  }

  public function load(ObjectManager $manager)
  {
   try
   {
       // Let's get the RESTHelper service.
       $ws = $this->container->get("chemical.helper");
       $periodicElements = $ws->getPeriodicElements();

       if (!empty($periodicElements)) {
           // Doctrine Manager is used to
           $em = $this->getDoctrine()->getManager();
           foreach ($periodicElements as $formula => $name) {
               // Declaration of a new Element entity.
               $element = new Element();
               $element->setFormula($formula);
               $element->setName($name);

               // Index the entity within Doctrine.
               // That method DOES NOT launch any request
               // to MYSQL server.
               $em->persist($element);
           }
           // Flush method sends all the stored requests to MySQL.
           $em->flush();
           // AddFlash is a method storing messages that are displayed once.
           // Check documentation to know how to display them within Twig template
           // (base.html.twig)
           $this->addFlash(
               'notice',
               'Atoms and periodic elements were created successfully.'
           );
       } else {
           $this->addFlash(
               'warning',
               'Nothing was acquired from webservice.'
           );
       }
   } catch (Exception $ex) {
       $this->addFlash(
           'error',
           'An error occurred while creating elements. Check the logs for more information.'
       );
       // Log that error.
       // $this->get('logger')->error($ex->getMessage());
   }
 }
}
*/
