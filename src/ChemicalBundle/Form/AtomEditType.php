<?php
// On a ici le principe d'heritage de formulaire. herite de DocumentEditType via la methode getParent.

namespace ChemicalBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AtomEditType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    //$builder->remove('formula');
  }

  /*public function getName()
  {
    return 'sns_platform_edit_annonce';
  }*/

  public function getParent()
  {
    return AtomType::class;
  }
}
