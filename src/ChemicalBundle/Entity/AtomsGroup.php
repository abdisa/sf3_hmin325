<?php

namespace ChemicalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * AtomsGroup
 *
 * @ORM\Table(name="chemical_atoms_group")
 * @ORM\Entity(repositoryClass="ChemicalBundle\Repository\AtomsGroupRepository")
 */
class AtomsGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\OneToOne(targetEntity="ChemicalBundle\Entity\Atom", cascade={"persist"})
     * @Assert\Valid()
     */
    private $mainAtom;

    /**
     * @ORM\OneToMany(targetEntity="ChemicalBundle\Entity\Atom", mappedBy="atomsGroup", cascade={"persist"})
     * @Assert\Valid()
     */
    private $atoms;

    /**
     * @ORM\ManyToOne(targetEntity="ChemicalBundle\Entity\Molecule", inversedBy="atomsGroups", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $molecule;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AtomsGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set mainAtom
     *
     * @param \ChemicalBundle\Entity\Atom $mainAtom
     *
     * @return AtomsGroup
     */
    public function setMainAtom(\ChemicalBundle\Entity\Atom $mainAtom = null)
    {
        $this->mainAtom = $mainAtom;

        return $this;
    }

    /**
     * Get mainAtom
     *
     * @return \ChemicalBundle\Entity\Atom
     */
    public function getMainAtom()
    {
        return $this->mainAtom;
    }

    /**
     * Add atom
     *
     * @param \ChemicalBundle\Entity\Atom $atoms
     *
     * @return AtomsGroup
     */
    public function addAtom(\ChemicalBundle\Entity\Atom $atom = null)
    {
        $this->atoms[] = $atom;

        return $this;
    }

    /**
     * Remove atom
     *
     * @param \ChemicalBundle\Entity\Atom $atom
     *
     */
    public function removeAtom(\ChemicalBundle\Entity\Atom $atom = null)
    {
        $this->atoms->removeElement($atom);
        return $this;
    }

    /**
     * Get atoms
     *
     * @return \ChemicalBundle\Entity\Atom
     */
    public function getAtoms()
    {
        return $this->atoms;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->atoms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set molecule
     *
     * @param \ChemicalBundle\Entity\Molecule $molecule
     *
     * @return AtomsGroup
     */
    public function setMolecule(\ChemicalBundle\Entity\Molecule $molecule = null)
    {
        $this->molecule = $molecule;

        return $this;
    }

    /**
     * Get molecule
     *
     * @return \ChemicalBundle\Entity\Molecule
     */
    public function getMolecule()
    {
        return $this->molecule;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return AtomsGroup
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
}
