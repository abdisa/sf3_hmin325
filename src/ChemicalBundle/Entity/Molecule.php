<?php

namespace ChemicalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Molecule
 *
 * @ORM\Table(name="chemical_molecule")
 * @ORM\Entity(repositoryClass="ChemicalBundle\Repository\MoleculeRepository")
 */
class Molecule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $formula;

    /**
     * @ORM\OneToMany(targetEntity="ChemicalBundle\Entity\AtomsGroup",mappedBy="molecule", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $atomsGroups;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set name
     *
     * @param string $name
     *
     * @return Molecule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add atomsGroup
     *
     * @param \ChemicalBundle\Entity\AtomsGroup $atomsGroups
     *
     * @return Molecule
     */
    public function addAtomsGroup(\ChemicalBundle\Entity\AtomsGroup $atomsGroup = null)
    {
        $this->atomsGroups[] = $atomsGroup;

        return $this;
    }

    /**
     * Remove atomsGroup
     *
     * @param \ChemicalBundle\Entity\AtomsGroup $atomsGroups
     *
     * @return Molecule
     */
    public function removeAtomsGroup(\ChemicalBundle\Entity\AtomsGroup $atomsGroup = null)
    {
        $this->atomsGroups->removeElement($atomsGroup);

        return $this;
    }

    /**
     * Get atomsGroups
     *
     * @return \ChemicalBundle\Entity\AtomsGroup
     */
    public function getAtomsGroups()
    {
        return $this->atomsGroups;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->atomsGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set formula
     *
     * @param string $formula
     *
     * @return Molecule
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }
}
