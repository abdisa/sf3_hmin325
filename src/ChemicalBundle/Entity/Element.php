<?php

namespace ChemicalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Element
 *
 * @ORM\Table(name="chemical_element")
 * @ORM\Entity(repositoryClass="ChemicalBundle\Repository\ElementRepository")
 */
class Element
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $formula;

    /**
     * @ORM\OneToMany(targetEntity="ChemicalBundle\Entity\Atom", mappedBy="element", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $atoms;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Element
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formula
     *
     * @param string $formula
     *
     * @return Element
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * That method is not created automatically.
     *
     * Used as a way to keep consistency across the application.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName() . '(' . $this->getFormula() . ')';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->atoms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add atom
     *
     * @param \ChemicalBundle\Entity\Atom $atom
     *
     * @return Element
     */
    public function addAtom(\ChemicalBundle\Entity\Atom $atom)
    {
        $this->atoms[] = $atom;

        return $this;
    }

    /**
     * Remove atom
     *
     * @param \ChemicalBundle\Entity\Atom $atom
     */
    public function removeAtom(\ChemicalBundle\Entity\Atom $atom)
    {
        $this->atoms->removeElement($atom);
    }

    /**
     * Get atoms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAtoms()
    {
        return $this->atoms;
    }
}
