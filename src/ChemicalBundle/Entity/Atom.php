<?php

namespace ChemicalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Atom
 *
 * @ORM\Table(name="chemical_atom")
 * @ORM\Entity(repositoryClass="ChemicalBundle\Repository\AtomRepository")
 */
class Atom
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="ChemicalBundle\Entity\Element",inversedBy="atoms", cascade={"persist"})
     * @Assert\Valid()
     */
    private $element;

    /**
     * @ORM\ManyToOne(targetEntity="ChemicalBundle\Entity\AtomsGroup", inversedBy="atoms", cascade={"persist"})
     * @Assert\Valid()
     */
    private $atomsGroup;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Atom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set element
     *
     * @param string $element
     *
     * @return Atom
     */
    public function setElement($element)
    {
        $this->element = $element;

        return $this;
    }

    /**
     * Get element
     *
     * @return string
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * Set atomsGroup
     *
     * @param \ChemicalBundle\Entity\AtomsGroup $atomsGroup
     *
     * @return Atom
     */
    public function setAtomsGroup(\ChemicalBundle\Entity\AtomsGroup $atomsGroup = null)
    {
        $this->atomsGroup = $atomsGroup;

        return $this;
    }

    /**
     * Get atomsGroup
     *
     * @return \ChemicalBundle\Entity\AtomsGroup
     */
    public function getAtomsGroup()
    {
        return $this->atomsGroup;
    }
}
