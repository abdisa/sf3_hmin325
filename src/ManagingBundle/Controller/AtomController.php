<?php

namespace ManagingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalBundle\Entity\Atom;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

use ChemicalBundle\Form\AtomType;
use ChemicalBundle\Form\AtomEditType;


class AtomController extends Controller
{
  /*
   * @Security("has_role('ROLE_ADMIN') ")
   */
  public function deleteAction($id, Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      // On récupère l'atom $id
      $atom = $em->getRepository('ChemicalBundle:Atom')->find($id);

      if (null === $atom)
      {
          throw new NotFoundHttpException("L' atom ".$id." n'existe pas.");
      }
      $form = $this->createFormBuilder()->getForm();

      if ($form->handleRequest($request)->isValid())
      {
        $em->remove($atom);
        $em->flush();

        $request->getSession()->getFlashBag()->add('info', "L'atom ".$id." a bien été supprimé.");

        return $this->redirect($this->generateUrl('chemical_atom_list'));
      }

      // Si la requête est en GET, on affiche une page de confirmation avant de supprimer
      return $this->render('chemical/atom.delete.html.twig', array(
          'atom' => $atom,
          'form'   => $form->createView()
            ));
  }

  /*
   * @Security("has_role('ROLE_USER') ")
   */
  public function addAction(Request $request)
	{

	   $atom = new Atom();

    $form = $this->get('form.factory')->create(AtomType::class, $atom);
    //$form = $this->createForm(ElementType::class, $element);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
  		// On l'enregistre notre objet dans la base de données
  		$em = $this->getDoctrine()->getManager();
  		$em->persist($atom);
  		$em->flush();

  		//$request->getSession()->getFlashBag()->add('notice', 'Document bien enregistré.');
      $this->addFlash(
        'notice',
        'New Atom has been added (id = ' . $atom->getId() . ' name = ' .$atom->getName() .')'
      );
  		// On redirige vers la page de visualisation de l'annonce nouvellement créée
  		return $this->redirect($this->generateUrl('chemical_atom_list'));
    }

    // À ce stade, le formulaire n'est pas valide
    return $this->render('chemical/atom.add.html.twig', array(
        'form'   => $form->createView()
        ));
  }

  /*
    * @Security("has_role('ROLE_ADMIN') ")
    */
  public function editAction($id, Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      // On récupère l'atom $id
      $atom = $em->getRepository('ChemicalBundle:Atom')->find($id);

      if (null === $atom)
      {
        throw new NotFoundHttpException("L'atom ".$id." n'existe pas.");
      }

      $form = $this->createForm(AtomEditType::class, $atom);

      if ($form->handleRequest($request)->isValid())
      {
        // Inutile de persister ici, Doctrine connait déjà notre annonce
        $em->flush();

        $request->getSession()->getFlashBag()->add('notice', 'l atom '.$id.' a bien été modifié.');

        return $this->redirect($this->generateUrl('chemical_atom_see', array('id' => $atom->getId())));
      }

      return $this->render('chemical/atom.edit.html.twig', array(
          'form'   => $form->createView(),
          'atom' => $atom // Je passe également l' atom à la vue si jamais elle veut l'afficher
            ));
  }
}
