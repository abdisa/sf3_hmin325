<?php

namespace ManagingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;



class UserController extends Controller
{
  public function listAction($id, Request $request)
  {
    $em = $this->getDoctrine()->getManager();

    $users = $em
                ->getRepository('ManagingBundle:User')
                ->findBy(
                          array(),
                          array('id' => 'ASC'),     // On trie par ordre croissante
                          null,                  // On sélectionne $limit annonces
                          null                        // À partir du premier
                          );

      return $this->render('user/list.html.twig', array(
            'users' => $users
            ));
  }

  public function setAdminAction($id)
  {
    $userManager = $this->get('fos_user.user_manager');
    $user = $userManager->findUserBy(array('id'=>$id));

    $user->addRole('ROLE_ADMIN');
    $userManager->updateUser($user);
    //$this->getDoctrine()->getManager()->flush();
    return $this->redirect($this->generateUrl('managing_user_list'));
  }

  public function unsetAdminAction($id)
  {
    $userManager = $this->get('fos_user.user_manager');
    $user = $userManager->findUserBy(array('id'=>$id));

    $user->removeRole('ROLE_ADMIN');
    $userManager->updateUser($user);
    //$this->getDoctrine()->getManager()->flush();
    return $this->redirect($this->generateUrl('managing_user_list'));
  }
}
