<?php

namespace ManagingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalBundle\Entity\Element;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

use ChemicalBundle\Form\ElementType;
use ChemicalBundle\Form\ElementEditType;


class ElementController extends Controller
{
  public function deleteAction($id, Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      // On récupère l'Element $id
      $element = $em->getRepository('ChemicalBundle:Element')->find($id);

      if (null === $element)
      {
          throw new NotFoundHttpException("L' element ".$id." n'existe pas.");
      }
      $form = $this->createFormBuilder()->getForm();

      if ($form->handleRequest($request)->isValid())
      {
        $em->remove($element);
        $em->flush();

        $request->getSession()->getFlashBag()->add('info', "L'element ".$id." a bien été supprimé.");

        return $this->redirect($this->generateUrl('chemical_element_list'));
      }

      // Si la requête est en GET, on affiche une page de confirmation avant de supprimer
      return $this->render('chemical/element.delete.html.twig', array(
          'element' => $element,
          'form'   => $form->createView()
            ));
  }

  /*
   * @Security("has_role('ROLE_USER') ")
   */
  public function addAction(Request $request)
	{

	   $element = new Element();

    $form = $this->get('form.factory')->create(ElementType::class, $element);
    //$form = $this->createForm(ElementType::class, $element);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
  		// On l'enregistre notre objet dans la base de données
  		$em = $this->getDoctrine()->getManager();
  		$em->persist($element);
  		$em->flush();

  		//$request->getSession()->getFlashBag()->add('notice', 'Document bien enregistré.');
      $this->addFlash(
        'notice',
        'New element has been added (id = ' . $element->getId() . ' name = ' .$element->getName() .')'
      );
  		// On redirige vers la page de visualisation de l'annonce nouvellement créée
  		return $this->redirect($this->generateUrl('chemical_element_list'));
    }

    // À ce stade, le formulaire n'est pas valide
    return $this->render('chemical/element.add.html.twig', array(
        'form'   => $form->createView()
        ));
  }

  public function editAction($id, Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      // On récupère l'Element $id
      $element = $em->getRepository('ChemicalBundle:Element')->find($id);

      if (null === $element)
      {
        throw new NotFoundHttpException("L'element ".$id." n'existe pas.");
      }

      $form = $this->createForm(ElementEditType::class, $element);

      if ($form->handleRequest($request)->isValid())
      {
        // Inutile de persister ici, Doctrine connait déjà notre annonce
        $em->flush();

        $request->getSession()->getFlashBag()->add('notice', 'l element '.$id.' a bien été modifié.');

        return $this->redirect($this->generateUrl('chemical_element_see', array('id' => $element->getId())));
      }

      return $this->render('chemical/element.edit.html.twig', array(
          'form'   => $form->createView(),
          'element' => $element // Je passe également l' Element à la vue si jamais elle veut l'afficher
            ));
  }
}
