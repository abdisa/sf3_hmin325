<?php

namespace ManagingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalBundle\Entity\Molecule;
use ChemicalBundle\Entity\Atom;
use ChemicalBundle\Entity\Element;
use ChemicalBundle\Entity\AtomsGroup;



use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

use ChemicalBundle\Form\MoleculeType;
use ChemicalBundle\Form\MoleculeEditType;


class MoleculeController extends Controller
{
  /*
   * @Security("has_role('ROLE_ADMIN') ")
   */
  public function deleteAction($id, Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      // On récupère l'Molecule $id
      $molecule = $em->getRepository('ChemicalBundle:Molecule')->find($id);

      if (null === $molecule)
      {
          throw new NotFoundHttpException("La molecule ".$id." n'existe pas.");
      }
      $form = $this->createFormBuilder()->getForm();

      if ($form->handleRequest($request)->isValid())
      {
        
        $em->remove($molecule);
        $em->flush();

        $request->getSession()->getFlashBag()->add('info', "La molecule ".$id." a bien été supprimé.");

        return $this->redirect($this->generateUrl('chemical_molecule_list'));
      }

      // Si la requête est en GET, on affiche une page de confirmation avant de supprimer
      return $this->render('chemical/molecule.delete.html.twig', array(
          'molecule' => $molecule,
          'form'   => $form->createView()
            ));
  }

  /*
   * @Security("has_role('ROLE_USER') ")
   */
  public function addAction(Request $request)
	{

	   $molecule = new Molecule();

    $form = $this->get('form.factory')->create(MoleculeType::class, $molecule);
    //$form = $this->createForm(moleculeType::class, $molecule);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
  		// On l'enregistre notre objet dans la base de données
  		$em = $this->getDoctrine()->getManager();
  		$em->persist($molecule);
  		$em->flush();

  		//$request->getSession()->getFlashBag()->add('notice', 'Document bien enregistré.');
      $this->addFlash(
        'notice',
        'New molecule has been added (id = ' . $molecule->getId() . ' name = ' .$molecule->getName() .')'
      );
  		// On redirige vers la page de visualisation de l'annonce nouvellement créée
  		return $this->redirect($this->generateUrl('chemical_molecule_list'));
    }

    // À ce stade, le formulaire n'est pas valide
    return $this->render('chemical/molecule.add.html.twig', array(
        'form'   => $form->createView()
        ));
  }

  /*
    * @Security("has_role('ROLE_ADMIN') ")
    */
  public function editAction($id, Request $request)
  {
      $em = $this->getDoctrine()->getManager();

      // On récupère l'molecule $id
      $molecule = $em->getRepository('ChemicalBundle:Molecule')->find($id);

      if (null === $molecule)
      {
        throw new NotFoundHttpException("La molecule ".$id." n'existe pas.");
      }

      $form = $this->createForm(MoleculeEditType::class, $molecule);

      if ($form->handleRequest($request)->isValid())
      {
        // Inutile de persister ici, Doctrine connait déjà notre annonce
        $em->flush();

        $request->getSession()->getFlashBag()->add('notice', 'la molecule '.$id.' a bien été modifié.');

        return $this->redirect($this->generateUrl('chemical_molecule_see', array('id' => $molecule->getId())));
      }

      return $this->render('chemical/molecule.edit.html.twig', array(
          'form'   => $form->createView(),
          'molecule' => $molecule // Je passe également la molecule à la vue si jamais elle veut l'afficher
            ));
  }

public function loadAction()
{
  //$this->generateElements();
  $this->generateMolecule();

  return $this->redirect($this->generateUrl('chemical_molecule_list'));
}

protected function generateElements()
{
    $ws = $this->container->get("chemical.helper");
    $periodicElements = $ws->getPeriodicElements();

    if (!empty($periodicElements)) {
      $em = $this->container->get('doctrine')->getManager();
      foreach ($periodicElements as $formula => $name) {
          $element = new Element();
          $element->setFormula($formula);
          $element->setName($name);
          $em->persist($element);
      }
      $em->flush();
    }
}

protected function generateMolecule()
{
    $em = $this->getDoctrine()->getManager();

    // On récupère l'Molecule $id
    $elementRepository = $em->getRepository('ChemicalBundle:Element');

    /*$em = $this->container->get('doctrine')->getManager();
    $elementRepository = $this
      ->container
      ->get('doctrine')
      ->getRepository('ChemicalBundle:Element');*/

    $molecule = new Molecule();
    $molecule->setFormula("CH4-NH2");
    $molecule->setName("Methane");

    /*$c = $elementRepository->findByFormula("C")[0]; //findOneByFormula("C");
    $h = $elementRepository->findByFormula("H")[0];
    $n = $elementRepository->findByFormula("N")[0];*/

    $c = $elementRepository->find(2);
    $h = $elementRepository->find(1);
    $n = $elementRepository->find(3);

    $cAtome = new Atom();
    $cAtome->setElement($c);
    $cAtome->setName($c->getName());

    $nAtome = new Atom();
    $nAtome->setElement($n);
    $nAtome->setName($n->getName());

    $atoms = [];
    $atoms2 = [];
      $atom2 = new Atom();
      $atom2->setElement($h);
      $atom2->setName($h->getName());
    $atoms2[] = $atom2;
    $atom2 = new Atom();
    $atom2->setElement($h);
    $atom2->setName($h->getName());
    $atoms2[] = $atom2;

    $atomsGroup = new AtomsGroup();
    $atomsGroup->setMainAtom($cAtome);
    $atomsGroup->setName("CH4");
    $atomsGroup2 = new AtomsGroup();
    $atomsGroup2->setName("NH2");
    $atomsGroup2->setMainAtom($nAtome);

    for ($i = 0; $i < 4; $i++) {
      $atoms[$i] = new Atom();
      $atoms[$i]->setElement($h);
      $atoms[$i]->setName($h->getName());
      $atoms[$i]->setAtomsGroup($atomsGroup);
    }
    foreach ($atoms as $atom) {
      $atomsGroup->addAtom($atom);
    }
    foreach ($atoms2 as $atom) {
      $atomsGroup2->addAtom($atom);
    }
    $atomsGroup->setMolecule($molecule);
    $atomsGroup->setPosition(1);
    $atomsGroup2->setMolecule($molecule);
    $atomsGroup2->setPosition(2);

    $molecule->addAtomsGroup($atomsGroup);
    $molecule->addAtomsGroup($atomsGroup2);

    $em->persist($atomsGroup);
    $em->persist($atomsGroup2);
    $em->persist($molecule);
    $em->flush();
}

}
